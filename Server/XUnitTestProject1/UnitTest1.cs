using Quest_1.Controllers;
using Quest_1.Models;
using System;
using Xunit;

namespace XUnitTestProject1
{
    public class UnitTest1
    {
        [Fact]
        public void BookingModel()
        {
            var booking = new Booking()
            {
                name = "Doa'a",
                mobile = "125487",
                note = "hi",
                email = "dd@xx.com",
            };

            Assert.Equal(booking.name , "Doa'a");
            Assert.Empty(booking.mobile);
        }

        [Fact]
        public void TimeSlotModel()
        {
            var time = new TimeSlot()
            {
                title = "9"
            };
            Assert.Equal(time.title, "9");

        }
    }
}
