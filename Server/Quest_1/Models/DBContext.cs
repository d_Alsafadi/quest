﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest_1.Models
{
    public class DBContext : DbContext
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Treatment>().ToTable("Treatment");
            modelBuilder.Entity<Booking>().ToTable("Booking");
            modelBuilder.Entity<TimeSlot>().ToTable("TimeSlot");

            modelBuilder.Entity<Booking>(entity =>
            {
                entity.HasKey(t => t.ID);

                entity.HasOne(e => e.time)
                .WithMany(e => e.bookings)
                .HasForeignKey(e => e.TimeSlotID)
                .IsRequired();

                entity.HasOne(e => e.treatment)
               .WithMany(e => e.bookings)
               .HasForeignKey(e => e.TreatmentID)
               .IsRequired();


            });
            modelBuilder.Entity<TimeSlot>(entity =>
            {
                entity.HasKey(t => t.ID);
            });

            modelBuilder.Entity<Treatment>(entity =>
            {
                entity.HasKey(t => t.ID);
            });
        }



    

    public DBContext(DbContextOptions<DBContext> options) : base(options)
        {
        }

        public DbSet<Treatment> Treatments { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<TimeSlot> Slots { get; set; }
    }

}



