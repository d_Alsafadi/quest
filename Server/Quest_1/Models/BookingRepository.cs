﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest_1.Models
{
    public class BookingRepository
    {
        private readonly DBContext _context;

        //public Booking book { get; set; }
        public BookingRepository(DBContext context)
        {
            _context = context;
        }

        public string Add(string userName, string email, string phone, string date, string note,string treatment , string time)
        {
            var addr = new System.Net.Mail.MailAddress(email);
            Console.WriteLine("validddddddddd mail.........");
            Console.WriteLine(addr.Address == email);

            if (_context.Bookings.SingleOrDefault(s => s.email == email) == null)
            {
                Console.WriteLine("xxxxxxxxxxxxxxxxxxxxxx...........inside ");
                //Console.WriteLine(x);
                Booking book = new Booking
                {
                    name = userName,

                    mobile = phone,
                    note = note,
                    email = email,
                    deleted = false,
                    date = date,
                    TreatmentID = _context.Treatments.Single(s => s.title == treatment).ID,
                    TimeSlotID = _context.Slots.Single(s => s.title == time).ID,
                };
                _context.Bookings.Add(book);
            _context.SaveChanges();
            }
            else
            {
                return "Email is already exist";
            }
            return "done";
        }

        public void Delete(string email)
        {
            var x = _context.Bookings.SingleOrDefault(s => s.email == email);
            x.deleted = true;
            _context.SaveChanges();
        }
        
        public Array getBooks()
        {
            var x = _context.Bookings.Where(s => s.deleted == false).ToArray();
            //Console.WriteLine("get dataaaaaaa  " + x[0]);
            return x;
        }

        public void UpdateAppointment(JObject data)
        {
            Console.WriteLine("I am UpdateAppointment ... ");

            var email = (string)data["email"];
            var note = (string)data["note"];
            Console.WriteLine("Treatment   " + (string)data["treatment"] + "   Slot   " + (string)data["time"]);
            var TreatmentID = _context.Treatments.Single(s => s.title == (string)data["treatment"]).ID;
            Console.Write("llllllllllllll   " + (string)data["time"]);
            var TimeSlotID = _context.Slots.Single(s => s.title == (string)data["time"]).ID;
            //var Treatment_Title = _context.Treatments.Single(s => s.title == (string)data["treatment"]).title;
            //var Time_Slot = _context.Treatments.Single(s => s.title == (string)data["treatment"]).title;


            Booking x = _context.Bookings.Single(s => s.email == email);
            x.note = note;
            x.TreatmentID = TreatmentID;
            x.TimeSlotID = TimeSlotID;

            _context.Update(x);
            _context.SaveChanges();
        }

        public Array getSeedData()
        {
            var treatments = _context.Treatments.ToArray();
            var slots = _context.Slots.ToArray();

            Console.WriteLine("treatments  " + treatments[0] + "slots  " + slots[0]);
            var TrSo = new object[treatments.Length + slots.Length];
            treatments.CopyTo(TrSo, 0);
            slots.CopyTo(TrSo, treatments.Length);
            Console.WriteLine("get dataaaaaaa  " + TrSo[0]);

            return TrSo;
        }
    }
}
