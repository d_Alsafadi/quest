﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest_1.Models
{
    public static class SeedData
    {
        public static void Initialize(DBContext context)
        {
            Console.WriteLine("i am seed data 2");

            context.Database.EnsureCreated();
            // Look for any students.
            if (context.Treatments.Any())
            {
                return;   // DB has been seeded
            }
            Console.WriteLine("i am seed data 3");

            var treatment = new Treatment[]
            {
            new Treatment{title="Alf Layla Wa Layla spa",description="relax on your own area"},
            new Treatment{title="Al-Pasha Turkish Bath spa",description="we take care of you"},
            new Treatment{title="Zara Spa",description="relax ..."},
            new Treatment{title="Ma'in Hot Springs",description="the best ever"},
            new Treatment{title="Nabatean Turkish bath ",description="......"},

            };
            foreach (var s in treatment)
            {
                context.Treatments.Add(s);
                context.SaveChanges();

            }

            var slot = new TimeSlot[]
            {
            new TimeSlot{title="9"},
            new TimeSlot{title="10"},
            new TimeSlot{title="11"},
            new TimeSlot{title="12"},
            new TimeSlot{title="13"},
            new TimeSlot{title="14"},
            new TimeSlot{title="15"},
            new TimeSlot{title="16"},
            new TimeSlot{title="17"},
            };
            foreach (var c in slot)
            {
                context.Slots.Add(c);
                context.SaveChanges();

            }


        }
    }
}
