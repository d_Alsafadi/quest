﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest_1.Models
{
    public class TimeSlot
    {
        public int ID { get; set; }
        public string title { get; set; }


        public ICollection<Booking> bookings { get; set; }
    }
}
