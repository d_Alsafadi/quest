﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest_1.Models
{
    public class Booking
    {
        //public enum Grade
        //{
        //    A, B, C, D, F
        //}
            public int ID { get; set; }
            public string name { get; set; }
            public string mobile { get; set; }
            public string note { get; set; }
            public string email { get; set; }
            public bool deleted { get; set; }
            public string date { get; set; }




            public int TreatmentID { get; set; }
            public int TimeSlotID { get; set; }

            public Treatment treatment { get; set; }
            public TimeSlot time { get; set; }
        
    }
}
