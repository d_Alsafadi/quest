﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Quest_1.Migrations
{
    public partial class add_new_Field : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "slot",
                table: "TimeSlot",
                newName: "title");

            migrationBuilder.AddColumn<string>(
                name: "description",
                table: "Treatment",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "date",
                table: "Booking",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "deleted",
                table: "Booking",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "email",
                table: "Booking",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "mobile",
                table: "Booking",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "note",
                table: "Booking",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "description",
                table: "Treatment");

            migrationBuilder.DropColumn(
                name: "date",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "deleted",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "email",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "mobile",
                table: "Booking");

            migrationBuilder.DropColumn(
                name: "note",
                table: "Booking");

            migrationBuilder.RenameColumn(
                name: "title",
                table: "TimeSlot",
                newName: "slot");
        }
    }
}
