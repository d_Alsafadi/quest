﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Quest_1.Models;


namespace Quest_1.Controllers
{
  // [EnableCors("AllowAllHeaders")]
    public class HomeController : Controller
    {
        public DBContext context;
        public HomeController(DBContext _context)
        {
            context = _context;
        }
        
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult BookAppointment()
        {
            ViewData["Message"] = "Your application description page.";
            return View();
        }

        [EnableCors("AllowAllHeaders")]
        [HttpPost]
        public string BookAppointment([FromBody] JObject data)
        {
            var book = new BookingRepository(context);
            Console.WriteLine(data);
            var x = book.Add((string)data["userName"], (string)data["email"], (string)data["phone"], (string)data["date"], (string)data["note"] , (string)data["treatment_Title"] , (string)data["time"]);
            return x;
        }

        [EnableCors("AllowAllHeaders")]
        [HttpPost]
        public void DeleteAppointment([FromBody] JObject data)
        {
            var b = new BookingRepository(context);
            b.Delete((string)data["email"]);
        }

        [HttpGet]
        public Array getBooks()
        {
            Console.WriteLine("I am getBooks lllllllllllllllllllll ");
            var x = new BookingRepository(context);
            var myData = x.getBooks();

            return myData;
        }

        [EnableCors("AllowAllHeaders")]
        [HttpPost]
        public void UpdateAppointment([FromBody] JObject data)
        {
            var t = data;
            Console.WriteLine("I am UpdateAppointment ... " + data["id"]);
            var x = new BookingRepository(context);
            x.UpdateAppointment(data);
        }

        [HttpGet]
        public Array getSeedData()
        {
            var x = new BookingRepository(context);
            var seedData = x.getSeedData();
            Console.WriteLine("seeeeeeeeeeeed" + seedData);
            return seedData;
        }

    }
}
