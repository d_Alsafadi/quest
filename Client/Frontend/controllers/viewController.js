(function () {

    var app = angular.module("MainModule");

    function homeFunction($http, viewBooks, $window, books) {

        var vm = this;
        vm.x = null;
        vm.books;
        vm.treatments;
        vm.slots;
        vm.TrSo;

        // grt all books from Server-----------------------------
        if (!vm.books) {
            $http.get("https://questdoaa.azurewebsites.net/Home/getBooks")
                .then(function (response) {
                    console.log("I get the books Data !!! Thank me.....:)   ")
                    vm.books = response.data
                    console.log(vm.books[vm.books.length - 1]);
                    //vm.books.forEach(addToBooks);
                    // get time and treatments from server----------------------
                    if (!vm.TrSo) {
                        $http.get("https://questdoaa.azurewebsites.net/Home/getSeedData")
                            .then(function (response) {
                                console.log("I get the books Data !!! Thank me.....:)   ")
                                vm.TrSo = response.data
                                //console.log(vm.TrSo);
                                vm.TrSo.forEach(splitArrays);
                                //    console.log(vm.treatments);                   
                            }, function () {
                                console.log("doneeeeeeeeeesh");
                            })

                    }
                }, function () {
                    console.log("doneeeeeeeeeesh");
                })

        }

        //delete Appointement from the DB------------------------------------
        vm.delete = function (obj) {
            var index = vm.books.indexOf(obj);
            vm.books.splice(index, 1);
            $http.post("https://questdoaa.azurewebsites.net/Home/DeleteAppointment", obj)
                .then(function (response) {
                    console.log("I post the Data !!! Thank me.....:)   ")

                })
        }

        //find user from his/her index
        vm.Index = function (index) {
            console.log(index)
            vm.x = vm.books[index];
            console.log(vm.x);
        }

        //update the data in DB
        vm.Save = function () {
            user = vm.x;
            $http.post("https://questdoaa.azurewebsites.net/Home/UpdateAppointment", user)
                .then(function (response) {
                    console.log("I Update the Data !!! Thank me.....:)   ");
                })
        }

        // function to split array to time and treatments-----------------------------------
        splitArrays = function (obj) {

            if (obj.title === "9") {
                var index = vm.TrSo.indexOf(obj);
                vm.treatments = vm.TrSo.slice(0, index);
                vm.slots = vm.TrSo.slice(index, vm.TrSo.length);
                vm.books.forEach(addToBooks);

                //    console.log(vm.slots[0])
            }
        }

        // function to add time and date to the books--------------------------------------------------
        var addToBooks = function (obj) {
            console.log(obj.id + "  " + vm.slots[0].id);
            for (var i = 0; i < vm.slots.length; i++) {
                if (obj.timeSlotID === vm.slots[i].id) {

                    obj.time = vm.slots[i].title;
                }
            }

            for (var i = 0; i < vm.treatments.length; i++) {
                if (obj.treatmentID === vm.treatments[i].id) {
                    console.log(vm.books)

                    obj.treatment = vm.treatments[i].title;
                }
            }

        }


    }

    app.controller("viewController", ["$http", 'viewBooks', "$window", "books", homeFunction]);

}())