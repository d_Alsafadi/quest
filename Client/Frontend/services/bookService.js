(function () {

    function homeService($http, $window, $location) {

        vm = this;

        // send book data to the server (---)(--)(-)---------------------------------------------------
        var Postbook = function (userName, email, phone, treatment, date, time, note) {
            var user = {
                userName: userName,
                email: email,
                phone: phone,
                treatment_Title: treatment,
                date: date,
                time: time,
                note: note
            }

            $http.post("https://questdoaa.azurewebsites.net/Home/BookAppointment", user)
                .then(function (response) {
                    console.log("your register successfully done")
                    console.log(response.data)
                    if (response.data === "Email is already exist") {
                        $window.alert("this E-mail is already exist !! Please try another Email");
                    } else {
                        $window.alert("your register successfuly done ... thank you <3");
                        $location.path("/view");
                    }
                }, function () {
                    console.log("something wrong Please try later !!!")
                })

            }

            // get time and treatments data-----------------------------------------------------------
            var getSeedData = function () {
                    $http.get("https://questdoaa.azurewebsites.net/Home/getSeedData")
                        .then(function (response) {
                            console.log("I get the books Data !!! Thank me.....:)   ")
                            vm.TrSo = response.data
                            console.log(vm.TrSo);
                            vm.TrSo.forEach(splitArrays);
                        }, function () {
                            console.log("doneeeeeeeeeesh");
                        })
            }

            // split array to time and treatments arrays----------------------------------------------
            var splitArrays = function (obj) {

                if (obj.title === "9") {
                    var index = vm.TrSo.indexOf(obj);
                    vm.treatments = vm.TrSo.slice(0, index);
                    vm.slots = vm.TrSo.slice(index, vm.TrSo.length);
                    console.log(vm.treatments);
                }
            }



      
        return {
            Postbook: Postbook,
            getSeedData: getSeedData,
        }
    }

    var app = angular.module("MainModule");

    app.factory('books', homeService)

}())